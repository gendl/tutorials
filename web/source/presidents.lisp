;;;; -*- coding: utf-8 -*-
;;
;; Copyright 2020 Genworks International
;;
;; This source file is part of the General-purpose Declarative
;; Language project (GDL).
;;
;; This source file contains free software: you can redistribute it
;; and/or modify it under the terms of the GNU Affero General Public
;; License as published by the Free Software Foundation, either
;; version 3 of the License, or (at your option) any later version.
;; 
;; This source file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;; 
;; You should have received a copy of the GNU Affero General Public
;; License along with this source file.  If not, see
;; <http://www.gnu.org/licenses/>.
;; 


(in-package :gwl-user)


(define-object presidents (base-ajax-sheet)

  :input-slots
  ((table-border 1)

   (data '((:name "Carter" :term 1976)
	   (:name "Reagan" :term 1980)
	   (:name "Bush" :term 1988)
	   (:name "Clinton" :term 1992)
	   (:name "Bush Jr"  :term 2000)
	   (:name "Obama" :term 2008)
	   (:name "Trump" :term 2016))))
  
  :computed-slots
  ((main-sheet-body
    (with-cl-who-string ()
      (str (the development-links))
      ((:table :border (the table-border))
       (dolist (president (list-elements (the presidents)))
	 (htm (:tr (:td ((:a :href (the-object  president url))
			 (str (the-object president name-input value))))
		   (:td (str (the-object president term-input value))))))))))
  
  :objects
  ((presidents :type 'president
	       :sequence (:size (length (the data)))
	       :pass-down (table-border)
	       :listing-page self
	       :data (nth (the-child index) (the data))
	       :pseudo-inputs (data)
	       :parameters (the-child data))))


(define-object president (base-ajax-sheet)

  :input-slots
  ((listing-page nil) (name "Carter") (term 1976) table-border)

  :computed-slots
  ((main-sheet-body
    (with-cl-who-string ()
      (when (the listing-page)
	(htm (:p ((:a :href (the listing-page url)) "&lt;-Back"))))
      (:p (str (the name-input html-string)))
      (:p (str (the term-input html-string)))
      (str (the main-sheet-section main-div)))))

  :objects
  ((main-sheet-section :type 'sheet-section
		       :inner-html
		       (with-cl-who-string ()
			 ((:table :border (the table-border))
			  (:tr (:th "Name") (:th "Term"))
			  (:tr (:td (str (the name-input value)))
			       (:td (str (the term-input value)))))))
   (name-input :type 'text-form-control
	       :ajax-submit-on-change? t
	       :default (the name))

   (term-input :type 'number-form-control
	       :ajax-submit-on-change? t
	       :default (the term))))



(publish-gwl-app "/pres" 'presidents)



;;
;; Following is a patch to fix the Update! error when using :parameters. 
;;

(in-package :gdl)

(defun %unbind-dependent-slots% (object slot &key updating?)
  (let ((current (gethash (list object slot) *unbound-slots*)))
    (unless current
      (setf (gethash (list object slot) *unbound-slots*) t)
      (if (slot-exists-p object slot)
          (let ((slot-value (slot-value object slot)))
            (when (not (eq (first (ensure-list slot-value)) 'gdl-rule:%unbound%))
              (when (and *eager-setting-enabled?* (null (second slot-value)))
                (push (list object slot) *leaf-resets*))
              (let ((list-or-hash (second slot-value)))
                (if (listp list-or-hash)
                    (mapc #'(lambda(notify-cons)
                              (destructuring-bind (node . messages) notify-cons
                                (mapc #'(lambda(message) 
                                          (%unbind-dependent-slots% node message 
                                                                    :updating? updating?)) 
                                      messages))) list-or-hash)
                    (maphash #'(lambda(node messages)
                                 (mapc #'(lambda(message) 
                                           (%unbind-dependent-slots%  node message 
                                                                      :updating? updating?)) 
                                       messages)) list-or-hash)))
              (if (third slot-value)
                  (setf (second (slot-value object slot)) nil)
                  (setf (slot-value object slot)
			(if (or updating? (not (the-object object remember-children?))
				(not *remember-previous-slot-values?*))
			    'gdl-rule::%unbound%
			    (list 'gdl-rule::%unbound% nil nil (first (slot-value object slot))))))))
          (when (and (find-class 'gdl-remote nil) (typep object 'gdl-remote))
            (the-object object (unbind-remote-slot slot)))))))
